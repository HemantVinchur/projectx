const mongoose = require('mongoose')
const Schema = mongoose.Schema

let skillsSchema = new Schema({
    domain: String,
    skills: [String],
    createdAt: { type: Date, default: Date.now() }
})

module.exports = mongoose.model('skillsSchema', skillsSchema)