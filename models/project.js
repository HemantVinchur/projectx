const mongoose = require('mongoose')
const Schema = mongoose.Schema
autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose);
let projectSchema = new Schema({
    projectId: { type: Number, default: null },
    projectTitle: { type: String, default: null },
    projectStatus: { type: String, default: null },
    description: { type: String, default: null },
    attachments: { type: String, default: null },
    comments: { type: String, default: null },
    refrence: { type: String, default: null },
    budget: { type: String, default: null },
    time: { type: String, default: null },
    technology: { type: String, default: null },
    createdAt: { type: Date, default: Date.now() }
})

projectSchema.plugin(autoIncrement.plugin,
    {
        model: 'projectSchema',
        field: 'projectId',
        startAt: 1,
        incrementBy: 1
    });

module.exports = mongoose.model('project', projectSchema)