const mongoose = require('mongoose')
const Schema = mongoose.Schema

let userSchema = new Schema({
    fullName: String,
    email: String,
    countryCode: { type: String, default: null },
    contactNo: { type: String, default: null },
    password: { type: String, default: null },
    address: { type: String, default: null },
    salt: { type: String, default: null },
    otp: { type: Number, default: null },
    createdAt: { type: Date, default: Date.now() }
})

module.exports = mongoose.model('userModel', userSchema)