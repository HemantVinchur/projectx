const functions = require('../function')
const user = require('../models/user')
const project = require('../models/project')
const skill = require('../models/skills')
const userAuth = require('../models/userAuth')
const jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
//......................................................User onboard started.................................................................................................................................................................................................................................................
//Signup:-

const userSignup = async (payLoad) => {
    try {
        console.log(payLoad)
        let findData = await user.findOne({ email: payLoad.email });
        console.log(findData)
        let mobileData = await user.findOne({ contactNo: payLoad.contactNo });
        console.log(mobileData + "..................................................................................................................................................................")
        // let hashObj = functions.hashPassword(payLoad.password)
        // console.log(hashObj)
        // delete payLoad.password
        // payLoad.salt = hashObj.salt
        // payLoad.password = hashObj.hash
        if (!findData) {
            if (!mobileData) {
                var userData = await user.create(payLoad);
                console.log(userData)
            } else {
                console.log("Contact no is already registered")
            }
        } else {
            console.log("Email is already registered")
        }
        console.log(userData)
        return { userData, findData, mobileData };
    } catch (error) {
        console.error(error)
        throw error
    }
}

//Signin:-

const userSignIn = async (payLoad) => {

    try {
        let data = await user.findOne({ email: payLoad.email });
        let authData = await userAuth.findOne({ username: payLoad.email });
        console.log(data)
        if (!data) {
            console.log("User not found")
        } else {
            var isPasswordValid = functions.validatePassword(data.salt, payLoad.password, data.password);
            console.log(isPasswordValid)
        }
        if (!isPasswordValid) {
            console.log("Incorrect password")
        } else if (authData) {
            console.log("Access token is already created.")
            var userInfo = { fullName: data.fullName, email: data.email, countryCode: data.countryCode, contactNo: data.contactNo, password: data.password, address: data.address, accessToken: authData.accessToken }
        } else {
            let token = jwt.sign({ email: payLoad.username }, 's3cr3t');
            console.log("Token-:", token)
            var userData = await userAuth.create({ email: payLoad.email, accessToken: token });
            var userInfo = { fullName: data.fullName, email: data.email, countryCode: data.countryCode, contactNo: data.contactNo, password: data.password, address: data.address, accessToken: userData.accessToken }
        }
        return { data, isPasswordValid, userInfo, authData }
    } catch (error) {
        console.log(error)
        throw error
    }
}

//Signup:-

const userGoogleSignIn = async (payLoad) => {
    try {
        var userData = await user.create({ email: payLoad.user_email, fullName: payLoad.user_name });
        console.log(userData)
        var authData = await userAuth.create({ email: payLoad.user_email, accessToken: payLoad.id_token });
        var newData = { email: payLoad.user_email, fullName: payLoad.user_name, accessToken: payLoad.id_token }
        return { userData, authData, newData };
    } catch (error) {
        console.error(error)
        throw error
    }
}

//Signin via access token:-

const signinViaTokenService = async (token) => {
    try {
        let decodedData = await functions.authenticate(token);
        console.log(decodedData)
        if (!decodedData) {
            console.log("Access token is not correct")
        }
        let userData = await userAuth.findOne({
            accessToken: token
        })
        if (!userData) {
            console.log("User not found")
        } else {
            let userInfo = await user.findOne({ email: userData.email });
            return { userInfo, decodedData, userData }
        }

    } catch (error) {
        console.error(error);
        throw error;
    }
}

//Reset password:-

const resetPassServices = async (payLoad, token) => {
    try {
        let find = await userAuth.findOne({ accessToken: token });
        if (find.email) {
            var findData = await user.findOne({ email: find.email });
            if (!findData) {
                return "User does not exist"
            } else {
                if (payLoad.password == payLoad.confirmPassword) {
                    let hashObj = functions.hashPassword(payLoad.password);
                    console.log(hashObj)
                    delete password;
                    payLoad.salt = hashObj.salt;
                    payLoad.password = hashObj.hash;
                    var updateData = await user.updateOne({ email: find.email },
                        {
                            $set: {
                                password: payLoad.password,
                                salt: payLoad.salt
                            }
                        },
                        { new: true });
                } else {
                    var confirm = "Confirm password is not correct"
                }
            }
        } else {
            return "User does not exists"
        }
        return { updateData, findData, confirm };
    } catch (error) {
        console.error(error);
        throw error;
    }
}

//Verify

const userVerify = async (payLoad) => {
    try {
        console.log(payLoad.email)
        let find = await user.findOne({ email: payLoad.email });
        if (find) {
            var otp = Math.floor(1000 + Math.random() * 9000);
            let createData = await user.updateOne({
                email: payLoad.email
            },
                {
                    otp: otp
                });
            if (createData) {
                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'venus.bityotta@gmail.com',
                        pass: 'venus@123'
                    }
                });

                const mailOptions = {
                    from: 'venus.bityotta@gmail.com',
                    to: payLoad.email,
                    subject: "OTP verification mail",
                    text: "Hi, " + "\n" + "Your OTP is " + otp
                };

                var sendMail = await transporter.sendMail(mailOptions);

            } else {
                var data = "OTP does not created"
            }
        } else {
            console.log("Username is invalid")
            var invalid = "Username is invalid"
        }
        return { find, data, invalid, sendMail }
    } catch (error) {
        console.error(error);
        throw error;
    }
}


//Verify OTP

const userVerifyOTP = async (payLoad) => {
    try {
        let find = await user.findOne({ otp: payLoad.otp });
        if (find) {
            console.log("otp is correct")
            var valid = "otp is correct"
        } else {
            console.log("otp is not correct")
            var invalid = "otp is not correct"
        }
        return { find, valid, invalid }
    } catch (error) {
        console.error(error);
        throw error;
    }
}

//Forgot password:-

const forgotPassServices = async (payLoad) => {
    try {
        var findData = await user.findOne({ email: payLoad.email });
        if (!findData) {
            return "User does not exist"
        } else {
            if (payLoad.password == payLoad.confirmPassword) {
                let hashObj = functions.hashPassword(payLoad.password);
                console.log(hashObj)
                delete password;
                payLoad.salt = hashObj.salt;
                payLoad.password = hashObj.hash;
                var updateData = await user.updateOne({ email: payLoad.email },
                    {
                        $set: {
                            password: payLoad.password,
                            salt: payLoad.salt
                        }
                    },
                    { new: true });
            } else {
                var confirm = "Confirm password is not correct"
            }
        }
        return { updateData, findData, confirm };
    } catch (error) {
        console.error(error);
        throw error;
    }
}

//User Logout

const userLogout = async (token) => {
    try {
        let decodeCode = await functions.authenticate(token)
        if (!decodeCode) {
            console.log("Access token not found")
        }
        token.accessToken = decodeCode.accessToken
        let deletetoken = await userAuth.deleteOne(token.accessToken)
        return deletetoken
    } catch (error) {
        console.log(error)
        throw error;
    }
}

//..................................................User onboard complete........................................................................................................................................................................................................................

//Project post API:-

const userProject = async (payLoad) => {
    try {
        var userData = await project.create(payLoad);
        return { userData }
    } catch (error) {
        console.error(error)
        throw error
    }
}


//Project post API:-

const userSkills = async (payLoad) => {
    try {
        var userData = await skill.create(payLoad);
        return { userData }
    } catch (error) {
        console.error(error)
        throw error
    }
}

//Get project

const getProject = async () => {
    console.log("Get project")
    try {
        console.log("getProject")
        let userData = await project.find();
        return userData;
    } catch (error) {
        console.error(error)
        throw error;
    }
}


//Get user

const getUsers = async () => {
    console.log("Get Users")
    try {
        console.log("getUsers")
        let userData = await user.find();
        return userData;
    } catch (error) {
        console.error(error)
        throw error;
    }
}


//Get project

const getUser = async (token) => {
    console.log("Get project")
    try {
        var findData = await userAuth.findOne({ accessToken: token });
        console.log("getProject")
        let userData = await user.findOne({ email: findData.email });
        return userData;
    } catch (error) {
        console.error(error)
        throw error;
    }
}

//Edit profile

const updateUser = async (payLoad, token) => {
    try {
        let find = await userAuth.findOne({ accessToken: token });
        console.log(find + ".........................................................................................................")
        console.log(find.email)
        if (find) {
            if (payLoad.name) {
                let userData = await user.updateOne({
                    email: find.email
                },
                    {
                        name: payLoad.name
                    });
            } if (payLoad.contactNo) {
                let userData = await user.updateOne({
                    email: find.email
                },
                    {
                        contactNo: payLoad.contactNo
                    });
                console.log(userData + ".......................................")
            } if (payLoad.address) {
                let userData = await user.updateOne({
                    email: find.email
                },
                    {
                        address: payLoad.address
                    });
            } if (payLoad.password) {
                // let hashObj = functions.hashPassword(payLoad.password)
                // console.log(hashObj)
                // delete payLoad.password
                // payLoad.salt = hashObj.salt
                // payLoad.password = hashObj.hash
                let userData = await user.updateOne({
                    email: find.email
                },
                    {
                        password: payLoad.password
                    });
            } else {
                var Invalid = "Invalid credentials";
            }
        } else {
            console.log("Access token is null")
            var accesstoken = "Access token is null"
        }
        let findData = await user.findOne({ email: find.email });
        return { findData, Invalid, accesstoken };
    } catch (error) {
        console.error(error)
        throw error

    }

}
//Update project

const updateProject = async (payLoad) => {
    try {
        if (payLoad.projectTitle) {
            var userData = await project.updateOne({
                projectId: payLoad.projectId
            },
                {
                    projectTitle: payLoad.projectTitle
                });
        } if (payLoad.projectStatus) {
            var userData = await project.updateOne({
                projectId: payLoad.projectId
            },
                {
                    projectStatus: payLoad.projectStatus
                });
        } if (payLoad.description) {
            var userData = await project.updateOne({
                projectId: payLoad.projectId
            },
                {
                    description: payLoad.description
                });
        } if (payLoad.attachments) {
            var userData = await project.updateOne({
                projectId: payLoad.projectId
            },
                {
                    attachments: payLoad.attachments
                });
        } else {
            var Invalid = "Invalid credentials";
        }
        let findData = await project.findOne({ projectId: payLoad.projectId });
        return { findData, Invalid };
    } catch (error) {
        console.error(error)
        throw error
    }

}

module.exports = { userSignup, userSignIn, userGoogleSignIn, signinViaTokenService, resetPassServices, userVerify, userVerifyOTP, forgotPassServices, userLogout, userProject, userSkills, getProject, getUsers, getUser, updateUser, updateProject }