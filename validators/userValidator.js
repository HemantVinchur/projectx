const { Joi, celebrate } = require('celebrate')
const signupReqValidator = celebrate({
    body: Joi.object().keys({
        fullName: Joi.string().required(),
        email: Joi.string().email().required(),
        countryCode: Joi.string().required(),
        contactNo: Joi.string().required(),
        password: Joi.string().required(),
        address: Joi.string().optional(),
    })
})

const signinReqValidator = celebrate({
    body: Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().required(),
    })
})

const googleSigninReqValidator = celebrate({
    body: Joi.object().keys({
        user_email: Joi.string().email().required(),
        user_name: Joi.string().required(),
        id_token: Joi.string().required(),
    })
})

const verifyValidator = celebrate({
    body: Joi.object().keys({
        email: Joi.string().email().required(),
    })
})

const verifyOTPValidator = celebrate({
    body: Joi.object().keys({
        otp: Joi.string().regex(/^[0-9]{4}$/).required(),
    })
})

const forgotPasswordValidator = celebrate({
    body: Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().required(),
        confirmPassword: Joi.string().required(),
    })
})

const resetPasswordValidator = celebrate({
    body: Joi.object().keys({
        password: Joi.string().required(),
        confirmPassword: Joi.string().required(),
    })
})

const projectValidator = celebrate({
    body: Joi.object().keys({
        projectTitle: Joi.string().required(),
        refrence: Joi.string().required(),
        currency: Joi.string().required(),
        budget: Joi.string().required(),
        time: Joi.string().required(),
        description: Joi.string().required(),
        technology: Joi.string().required(),
        url: Joi.string().optional(),
    })
})

const skillsValidator = celebrate({
    body: Joi.object().keys({
        domain: Joi.string().required(),
        skills: Joi.array().items(Joi.string())
    })
})

module.exports = { signupReqValidator, signinReqValidator, googleSigninReqValidator, verifyValidator, verifyOTPValidator, forgotPasswordValidator, resetPasswordValidator, projectValidator, skillsValidator }