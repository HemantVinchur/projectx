const router = require('express').Router();
const jwt = require('jsonwebtoken')
const functions = require('../function')
const userValidator = require('../validators/userValidator.js')
const services = require('../services/userServices')
console.log("userRoutes....................")

//Signup

router.post('/signup', userValidator.signupReqValidator,
    async (req, res) => {
        try {
            let payLoad = req.body;
            let newData = await services.userSignup(payLoad);
            console.log(newData)
            if (newData.userData) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "Signup successful",
                    data: newData.userData
                })
            } else if (newData.findData) {
                return res.status(200).json({
                    statusCode: 403,
                    message: "Email already exists",
                    data: {}
                })
            } else if (newData.mobileData) {
                return res.status(200).json({
                    statusCode: 403,
                    message: "Mobile no. already exists",
                    data: {}
                })
            } else {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Signup unsuccessful",
                    data: {}
                })
            }
        } catch (error) {
            console.log(error)
            res.status(200).json({
                statusCode: 500,
                message: "Signup unsuccessful",
                data: {}
            })


        }

    })


//Fetch user
router.get('/getUser',
    async (req, res) => {
        try {
            if (!req.headers.authorization) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Access Token not found",
                    data: {}
                })
            }
            let token = req.headers.authorization.split(' ')[1];
            console.log("User get API")
            let newData = await services.getUser(token);
            return res.status(200).json({
                statusCode: 200,
                message: "User successfully fetched",
                data: newData
            })

        } catch (error) {
            res.status(200).json({
                statusCode: 400,
                message: "Users does not fetched",
                data: {}
            })


        }

    })


//Fetch user
router.get('/getUsers',
    async (req, res) => {
        try {
            console.log("User get API")
            let newData = await services.getUsers();
            return res.status(200).json({
                statusCode: 200,
                message: "All users successfully fetched",
                data: newData
            })

        } catch (error) {
            res.status(200).json({
                statusCode: 400,
                message: "Users does not fetched",
                data: {}
            })


        }

    })

//Signin

router.post('/signin', userValidator.signinReqValidator,
    async (req, res) => {
        try {
            let payLoad = req.body;
            let newData = await services.userSignIn(payLoad);
            console.log(newData)
            console.log(newData.data)
            if (newData.userInfo) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "SignIn successful",
                    data: newData.userInfo
                })
            } else if (!newData.data) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "User not found",
                    data: {}
                })
            } else if (!newData.isPasswordValid) {
                return res.status(200).json({
                    statusCode: 401,
                    message: "Incorrect password",
                    data: {}
                })
            } else {
                return res.status(200).json({
                    statusCode: 400,
                    message: "Signin unsuccessful",
                    data: {}
                })
            }
        } catch (error) {
            console.log(error)
            res.status(200).json({
                statusCode: 400,
                message: "Signin unsuccessful",
                data: {}
            })
        }

    })


//Google signin

router.post('/googleSignin', userValidator.googleSigninReqValidator,
    async (req, res) => {
        try {
            let payLoad = req.body;
            let data = await services.userGoogleSignIn(payLoad);
            if (data.userData) {
                if (data.authData) {
                    return res.status(200).json({
                        statusCode: 200,
                        message: "SignIn successful",
                        data: data.newData
                    })
                } else {
                    return res.status(200).json({
                        statusCode: 404,
                        message: "SignIn unsuccessful",
                        data: {}
                    })
                }
            } else {
                return res.status(200).json({
                    statusCode: 404,
                    message: "SignIn unsuccessful",
                    data: {}
                })
            }
        } catch (error) {
            console.log(error)
            res.status(200).json({
                statusCode: 400,
                message: "Signin unsuccessful",
                data: {}
            })
        }

    })


//Signin via accesstoken

router.post('/signinviatoken',
    async (req, res) => {
        try {
            if (!req.headers.authorization) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Access Token not found",
                    data: {}
                })
            }
            let token = req.headers.authorization.split(' ')[1];
            console.log(token);
            let data = await services.signinViaTokenService(token);
            console.log("...................................." + data.userInfo + ".............................")
            if (data.userInfo) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "Signin successful",
                    data: data.userInfo
                })
            } else if (!data.userData) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "User not found",
                    data: {}
                })
            } else if (!data.decodedData) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Access token is not correct",
                    data: {}
                })
            } else {
                return res.status(200).json({
                    statusCode: 400,
                    message: "Signin unsuccessful",
                    data: {}
                })
            }
        } catch (error) {
            return res.status(200).json({
                statusCode: 401,
                message: "Access token is not correct",
                data: {}
            })
        }
    })

//User reset password
router.put('/resetPassword', userValidator.resetPasswordValidator,
    async (req, res) => {
        try {

            if (!req.headers.authorization) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Access Token not found",
                    data: {}
                })
            }
            let token = req.headers.authorization.split(' ')[1];
            console.log(token)
            let decodeCode = await functions.authenticate(token)
            if (!decodeCode) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Access Token not found",
                    data: {}
                })
            }
            token.accessToken = decodeCode.accessToken
            let payLoad = req.body;
            let newData = await services.resetPassServices(payLoad, token);
            console.log(newData)
            if (newData.updateData) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "Password successfully updated",
                    data: newData.updateData
                })
            } else if (!newData.findData) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "User does not exists",
                    data: {}
                })
            }
            else if (newData.confirm) {
                return res.status(200).json({
                    statusCode: 401,
                    message: "Confirm password is not correct",
                    data: {}
                })
            } else {
                return res.status(200).json({
                    statusCode: 400,
                    message: "Password does not updated",
                    data: {}
                })
            }
        }
        catch (error) {
            console.log(error);
            res.status(200).json({
                statusCode: 400,
                message: "Password does not updated",
                data: {}
            })
        }
    })

//Verify

router.post('/verify', userValidator.verifyValidator,
    async (req, res) => {
        try {
            let payLoad = req.body;
            console.log(payLoad);
            let findData = await services.userVerify(payLoad);
            if (findData.sendMail) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "Mail successfully send",
                    data: findData.sendMail
                })
            } else if (findData.invalid) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Username is invalid",
                    data: {}
                })
            } else if (findData.data) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "OTP does not created",
                    data: {}
                })
            } else {
                return res.status(200).json({
                    statusCode: 400,
                    message: "Mail does not send",
                    data: {}
                })
            }
        }
        catch (error) {
            console.log(error);
            res.status(200).json({
                statusCode: 400,
                message: "Failure",
                data: {}
            })
        }
    })


//Verify

router.post('/verifyOTP', userValidator.verifyOTPValidator,
    async (req, res) => {
        try {
            let payLoad = req.body;
            console.log(payLoad);
            let findData = await services.userVerifyOTP(payLoad);
            if (findData.valid) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "otp is correct",
                    data: findData.find
                })
            } else if (findData.invalid) {
                return res.status(200).json({
                    statusCode: 401,
                    message: "otp is not correct",
                    data: {}
                })
            } else {
                return res.status(200).json({
                    statusCode: 400,
                    message: "otp verification fail",
                    data: {}
                })
            }
        }
        catch (error) {
            console.log(error);
            res.status(200).json({
                statusCode: 400,
                message: "otp verification fail",
                data: {}
            })
        }
    })

//User forgot password
router.post('/forgotPassword', userValidator.forgotPasswordValidator,
    async (req, res) => {
        try {
            let payLoad = req.body;
            let newData = await services.forgotPassServices(payLoad);
            if (newData.updateData) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "Password successfully updated",
                    data: newData.updateData
                })
            } else if (!newData.findData) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "User does not exists",
                    data: {}
                })
            } else if (newData.confirm) {
                return res.status(200).json({
                    statusCode: 401,
                    message: "Confirm password is not correct",
                    data: {}
                })
            } else {
                return res.status(200).json({
                    statusCode: 400,
                    message: "Password does not updated",
                    data: {}
                })
            }
        }
        catch (error) {
            console.log(error);
            res.status(200).json({
                statusCode: 400,
                message: "Password does not updated",
                data: {}
            })
        }
    })


//Update user-:

router.put('/updateUser',
    async (req, res) => {
        try {
            if (!req.headers.authorization) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Access Token not found",
                    data: {}
                })
            }
            let token = req.headers.authorization.split(' ')[1];
            console.log(token)
            let decodeCode = await functions.authenticate(token)
            if (!decodeCode) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Access Token not found",
                    data: {}
                })
            }

            token.accessToken = decodeCode.accessToken
            let payLoad = req.body;
            let newData = await services.updateUser(payLoad, token);
            console.log(newData)
            if (newData.findData) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "Profile successfully updated",
                    data: newData.findData
                })
            } else if (newData.Invalid) {
                return res.status(200).json({
                    statusCode: 401,
                    message: "Invalid credentials",
                    data: {}
                })
            } else if (newData.invalid) {
                return res.status(200).json({
                    statusCode: 401,
                    message: "Access token or email is not correct",
                    data: {}
                })
            } else if (newData.accesstoken) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Access token not found",
                    data: {}
                })
            } else {
                return res.status(200).json({
                    statusCode: 401,
                    message: "Profile does not updated",
                    data: {}
                })
            }
        } catch (error) {
            console.log(error)
            res.status(200).json({
                statusCode: 400,
                message: "Profile does not updated",
                data: {}
            })


        }

    })

//User Logout

router.post('/logout',
    async (req, res) => {
        try {
            if (!req.headers.authorization) {
                return res.status(200).json({
                    statusCode: 404,
                    message: "Access Token not found",
                    data: {}
                })
            }
            let token = req.headers.authorization.split(' ')[1];
            console.log(token);

            let data = await services.userLogout(token);
            if (data) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "Logout successful",
                    data: data
                })
            } else {
                return res.status(200).json({
                    statusCode: 400,
                    message: "Logout unsuccessful",
                    data: data
                })
            }
        } catch (error) {
            return res.status(200).json({
                statusCode: 401,
                message: "Access token is not correct",
                data: {}
            })
        }
    })
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||Onboard completed|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


//Project post API

router.post('/project', userValidator.projectValidator,
    async (req, res) => {
        try {
            let payLoad = req.body;
            let newData = await services.userProject(payLoad);
            console.log(newData)
            if (newData.userData) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "Project successfully added",
                    data: newData.userData
                })
            } else {
                return res.status(200).json({
                    statusCode: 400,
                    message: "Project does not added",
                    data: {}
                })
            }
        } catch (error) {
            console.log(error)
            res.status(200).json({
                statusCode: 500,
                message: "Project does not added",
                data: {}
            })


        }

    })



//Add skills API

router.post('/skills', userValidator.skillsValidator,
    async (req, res) => {
        try {
            let payLoad = req.body;
            let newData = await services.userSkills(payLoad);
            console.log(newData)
            if (newData.userData) {
                return res.status(200).json({
                    statusCode: 200,
                    message: "Project successfully added",
                    data: newData.userData
                })
            } else {
                return res.status(200).json({
                    statusCode: 400,
                    message: "Project does not added",
                    data: {}
                })
            }
        } catch (error) {
            console.log(error)
            res.status(200).json({
                statusCode: 500,
                message: "Project does not added",
                data: {}
            })


        }

    })


//Fetch project
router.get('/getProject',
    async (req, res) => {
        try {
            console.log("Project get API")
            let newData = await services.getProject();
            return res.status(200).json({
                statusCode: 200,
                message: "All projects successfully fetched",
                data: newData
            })

        } catch (error) {
            res.status(200).json({
                statusCode: 400,
                message: "Projects does not fetched",
                data: {}
            })


        }

    })


//Update project

router.put('/updateProject',
    async (req, res) => {
        try {
            console.log("Project update API")
            let payLoad = req.body;
            let newData = await services.updateProject(payLoad);
            console.log(newData)
            if(newData.findData){
            res.status(200).json({
                statusCode: 200,
                message: "Project successfully updated",
                data: newData.findData
            })
        }else if(!newData.findData){
            res.status(200).json({
                statusCode: 404,
                message: "Project does not exists",
                data: {}
            })
        }else{
            res.status(200).json({
                statusCode: 400,
                message: "Project does not updated",
                data: {}
            })
        }
        } catch (error) {
            res.status(200).json({
                statusCode: 400,
                message: "Project does not updated",
                data: {}
            })
        }

    })

module.exports = router;